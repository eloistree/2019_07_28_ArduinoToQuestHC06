# How to use: 2019_07_28_ArduinoToQuestHC06   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.arduinotoquesthc06":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.arduinotoquesthc06",                              
  "displayName": "Arduino To Quest HC 06",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Script and tools to send information to the Quest from Arduino with Bluetooth",                         
  "keywords": ["Script","Tool","Arduino","Bluetooth","Android"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    