/**
 * Exemple de code pour lire un unique capteur DS18B20 sur un bus 1-Wire.
 */
#include "Arduino.h"
#include <SoftwareSerial.h>
/* Dépendance pour le bus 1-Wire */
#include <OneWire.h>
 
 
/* Broche du bus 1-Wire */
const byte BROCHE_ONEWIRE = 7;
SoftwareSerial BTserial(4, 6);
byte LEDpin = 8;
/* Code de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
  READ_OK,  // Lecture ok
  NO_SENSOR_FOUND,  // Pas de capteur
  INVALID_ADDRESS,  // Adresse reçue invalide
  INVALID_SENSOR  // Capteur invalide (pas un DS18B20)
};


/* Création de l'objet OneWire pour manipuler le bus 1-Wire */
OneWire ds(BROCHE_ONEWIRE);
 
 
/**
 * Fonction de lecture de la température via un capteur DS18B20.
 */
byte getTemperature(float *temperature, byte reset_search) {
  byte data[9], addr[8];
  // data[] : Données lues depuis le scratchpad
  // addr[] : Adresse du module 1-Wire détecté
  
  /* Reset le bus 1-Wire ci nécessaire (requis pour la lecture du premier capteur) */
  if (reset_search) {
    ds.reset_search();
  }
 
  /* Recherche le prochain capteur 1-Wire disponible */
  if (!ds.search(addr)) {
    // Pas de capteur
    return NO_SENSOR_FOUND;
  }
  
  /* Vérifie que l'adresse a été correctement reçue */
  if (OneWire::crc8(addr, 7) != addr[7]) {
    // Adresse invalide
    return INVALID_ADDRESS;
  }
 
  /* Vérifie qu'il s'agit bien d'un DS18B20 */
  if (addr[0] != 0x28) {
    // Mauvais type de capteur
    return INVALID_SENSOR;
  }
 
  /* Reset le bus 1-Wire et sélectionne le capteur */
  ds.reset();
  ds.select(addr);
  
  /* Lance une prise de mesure de température et attend la fin de la mesure */
  ds.write(0x44, 1);
  delay(800);
  
  /* Reset le bus 1-Wire, sélectionne le capteur et envoie une demande de lecture du scratchpad */
  ds.reset();
  ds.select(addr);
  ds.write(0xBE);
 
 /* Lecture du scratchpad */
  for (byte i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
   
  /* Calcul de la température en degré Celsius */
  *temperature = (int16_t) ((data[1] << 8) | data[0]) * 0.0625; 
  
  // Pas d'erreur
  return READ_OK;
}
 
 
/** Fonction setup() **/
void setup() {

  /* Initialisation du port série */
  Serial.begin(9600);
  BTserial.begin(9600);
  digitalWrite(3, HIGH);
  digitalWrite(LEDpin, HIGH);
}
 
 
/** Fonction loop() **/
void loop() {

if (BTserial.available() > 0) {

    // Get the char
    char data = (char) BTserial.read();

    // Depending on value turn LED ON or OFF (or error message)
    switch (data) {

    case '1':
      Serial.println("ON");
      digitalWrite(LEDpin, HIGH);
      BTserial.print("ON");
      break;

    case '0':
      Serial.println("OFF");
      digitalWrite(LEDpin, LOW);
      BTserial.print("OFF");
      break;

    default:
      Serial.print("NOT RECOGNISED: ");
      Serial.println(data);
      BTserial.print("Error!");
    }
  }
  
  float temperature;
   
  /* Lit la température ambiante à ~1Hz */
  if (getTemperature(&temperature, true) != READ_OK) {
    Serial.println(F("Erreur de lecture du capteur"));
    return;
  }

  /* Affiche la température */
  //Serial.print(F("Temperature : "));
  Serial.print(temperature, 2);
  Serial.write('C');
//  BTserial.write(String(temperature)+"C");
String s = "temperature:"+String(temperature)+"\n";
   BTserial.print(s);
  //Serial.write(176); // Caractère degré
  Serial.println();


}
